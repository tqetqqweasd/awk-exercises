BEGIN {MAX = 0; LINE=0}

{
    buffer[LINE] = $0;
    LINE++;
    if (NF > MAX)
	MAX = NF;
}

END {
    for (i=0; i < LINE; i++)
    {
	$0 = buffer[i];
	SUM = 0
	for (j=1; j<MAX+1; j++)
	{
	    printf("|%5d", $j );
	    SUM += $j;
	}
	printf("|%4d|\n", SUM);
	    
    }
}
