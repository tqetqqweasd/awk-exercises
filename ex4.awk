BEGIN {PATTERN[0] = 0; PATTERNS=0; STRING[0] = 0;}

/#replace/ {
    
    if (NF > 2)
    {
	PATTERN[PATTERNS] = $2;
	STRING[PATTERNS] = $3;
	PATTERNS++;
	next;
    }
}

{
    for (i=0;i<PATTERNS;i++)
    {
	gsub(PATTERN[i], STRING[i], $0)
    }
    print $0;
}
